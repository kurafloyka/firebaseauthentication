package com.example.firebaseauthentication.practise5;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.firebaseauthentication.R;
import com.example.firebaseauthentication.practise1.HomePageActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ChangePasswordScreenActivity extends AppCompatActivity {

    EditText changeEditText;
    Button sendNewPassword;
    FirebaseAuth auth;
    FirebaseUser user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password_screen);
        define();

        sendNewPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePassword(changeEditText.getText().toString());
            }
        });

    }


    void define() {

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        changeEditText = findViewById(R.id.changeEditText);
        sendNewPassword = findViewById(R.id.sendNewPassword);
    }


    void changePassword(String pass) {


        user.updatePassword(pass).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Intent intent = new Intent(getApplicationContext(), HomePageActivity.class);
                    startActivity(intent);
                    finish();
                    Log.i("TO","TO");
                }
            }
        });
    }
}