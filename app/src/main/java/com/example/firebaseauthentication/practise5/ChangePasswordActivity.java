package com.example.firebaseauthentication.practise5;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.firebaseauthentication.R;
import com.example.firebaseauthentication.practise3.LoginActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class ChangePasswordActivity extends AppCompatActivity {


    FirebaseAuth auth;
    FirebaseUser user;
    Button exitButton, changePasswordButton, sendValidation, sendEmailResetPassword, deleteUser;
    TextView validateTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);


        define();

        if (user == null) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        } else {
            Log.i("ID : ", user.getUid());

        }

        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                auth.signOut();
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        changePasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ChangePasswordScreenActivity.class);
                startActivity(intent);
                finish();
            }
        });

        validateTextView.setText("Validate Email : " + user.isEmailVerified());


        sendValidation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendValidateEmail();
                //Intent intent = new Intent(getApplicationContext(), ValidationEmailActivity.class);
                //startActivity(intent);
                //finish();
            }
        });


        sendEmailResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSendEmailResetPassword();
            }
        });

        deleteUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDeleteUser();
            }
        });
    }


    void define() {
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        exitButton = findViewById(R.id.exit);
        changePasswordButton = findViewById(R.id.changePassword);
        validateTextView = findViewById(R.id.validateEmail);
        sendValidation = findViewById(R.id.sendValidation);
        sendEmailResetPassword = findViewById(R.id.sendEmailResetPassword);
        deleteUser = findViewById(R.id.deleteUser);
    }


    void sendValidateEmail() {

        user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

            }
        });
    }


    void setSendEmailResetPassword() {
        auth.sendPasswordResetEmail(user.getEmail()).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(getApplicationContext(), "Was already Sent reset Link....", Toast.LENGTH_LONG).show();
            }
        });
    }

    void setDeleteUser() {


        user.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if (task.isSuccessful()) {
                    Log.i("DELETE","DELETE");
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                    finish();
                }

            }
        });


    }
}
