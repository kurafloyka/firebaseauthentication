package com.example.firebaseauthentication.practise1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.firebaseauthentication.R;

public class HomePageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }
}
