package com.example.firebaseauthentication.practise1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.firebaseauthentication.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {
    FirebaseAuth auth;
    FirebaseUser user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();


        if (user == null) {

            Intent intent = new Intent(this, HomePageActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(), user.getUid(), Toast.LENGTH_LONG).show();
        }
    }
}
