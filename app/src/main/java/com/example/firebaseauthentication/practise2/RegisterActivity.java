package com.example.firebaseauthentication.practise2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.firebaseauthentication.R;
import com.example.firebaseauthentication.practise1.HomePageActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class RegisterActivity extends AppCompatActivity {


    EditText gmail, password;
    Button register;
    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        define();

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mail, pass;
                mail = gmail.getText().toString();
                pass = password.getText().toString();

                Log.i("Test2", mail + pass);

                if (!mail.equals("") && !pass.equals("")) {
                    registerApp(mail, pass);
                    gmail.setText("");
                    password.setText("");
                }

            }
        });

    }


    public void define() {
        gmail = findViewById(R.id.gmail);
        password = findViewById(R.id.password);
        register = findViewById(R.id.register);
        auth = FirebaseAuth.getInstance();
    }

    private void registerApp(String email, String pass) {


        auth.createUserWithEmailAndPassword(email, pass).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {


                if (task.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "Successed....", Toast.LENGTH_LONG).show();


                    Intent intent = new Intent(getApplicationContext(), HomePageActivity.class);
                    startActivity(intent);
                } else {


                    Toast.makeText(getApplicationContext(), "Failured", Toast.LENGTH_LONG).show();
                }


            }
        });


    }


}
