package com.example.firebaseauthentication.practise3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.firebaseauthentication.R;
import com.example.firebaseauthentication.practise1.HomePageActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity {


    EditText userMail, password;
    Button logInButton;
    FirebaseAuth auth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        define();


        logInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mail = userMail.getText().toString();
                String pass = password.getText().toString();


                if (!mail.equals("") && !pass.equals("")) {
                    userMailAndPassword(mail, pass);
                } else {

                    Toast.makeText(getApplicationContext(), "UserInfo is incorrect....", Toast.LENGTH_LONG).show();
                }


            }
        });


    }

    private void define() {

        userMail = findViewById(R.id.userMail);
        password = findViewById(R.id.userPass);
        logInButton = findViewById(R.id.logInButton);
        auth = FirebaseAuth.getInstance();
    }


    private void userMailAndPassword(String mail, String pass) {


        auth.signInWithEmailAndPassword(mail, pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()) {
                    Log.i("TAGS", "TEST");
                    Intent intent = new Intent(LoginActivity.this, HomePageActivity.class);
                    startActivity(intent);
                    finish();


                } else {


                    Toast.makeText(getApplicationContext(), "LogIn is unsuccessful.", Toast.LENGTH_LONG).show();
                }

            }
        });
    }
}
