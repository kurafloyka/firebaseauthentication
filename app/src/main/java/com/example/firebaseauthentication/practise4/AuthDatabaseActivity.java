package com.example.firebaseauthentication.practise4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.firebaseauthentication.R;
import com.example.firebaseauthentication.practise2.RegisterActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class AuthDatabaseActivity extends AppCompatActivity {


    FirebaseAuth auth;
    FirebaseUser user;

    FirebaseDatabase database;
    DatabaseReference reference;

    TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth_database);
        define();


        if (user == null) {
            Intent intent = new Intent(this, RegisterActivity.class);
            startActivity(intent);
            finish();
        } else {

            Toast.makeText(getApplicationContext(), user.getUid(), Toast.LENGTH_LONG).show();
        }


        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add();
            }
        });
    }

    public void define() {

        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();


        database = FirebaseDatabase.getInstance();
        reference = database.getReference("bilgi/" + user.getUid());

        textView = findViewById(R.id.bilgi);


    }


    void add() {

        Map map = new HashMap();
        map.put("boy", "187");
        map.put("yas", "28");
        reference.setValue(map);
    }


}
